(defsystem "logging-caveman-test"
  :defsystem-depends-on ("prove-asdf")
  :author "Santiago Payà Miralta"
  :license "MIT"
  :depends-on ("logging-caveman"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "logging-caveman"))))
  :description "Test system for logging-caveman"
  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
