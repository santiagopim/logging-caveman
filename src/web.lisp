(in-package :cl-user)
(defpackage logging-caveman.web
  (:use :cl
        :logging-caveman.config
        :logging-caveman.view
        :logging-caveman.db)
  (:export :*web*))
(in-package :logging-caveman.web)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Application

(defclass <web> (caveman2:<app>) ())
(defvar *web* (make-instance '<web>))
(caveman2:clear-routing-rules *web*)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; WebSocket
(defvar *connections* (make-hash-table))

(defun handle-new-connection (con)
  (setf (gethash con *connections*)
        (format nil "User ~A" (random 1000000))))

(defun broadcast-to-room (connection message)
  (let ((message (format nil "~A: ~A"
                         (gethash connection *connections*)
                         message)))
    (loop :for con :being :the :hash-key :of *connections* :do
      (websocket-driver:send con message))))

(defun handle-close-connection (connection)
  (let ((message (format nil " ... ~A has left."
                         (gethash connection *connections*))))
    (remhash connection *connections*)
    (loop :for con :being :the :hash-key :of *connections* :do
      (websocket-driver:send con message))))

;;; Define server

(defun chat-server (env)
  (let ((ws (websocket-driver:make-server env)))
    (websocket-driver:on :open ws
                         (lambda () (handle-new-connection ws)))
    (websocket-driver:on :message ws
                         (lambda (msg) (broadcast-to-room ws msg)))
    (websocket-driver:on :close ws
                         (lambda (&key code reason)
                           (declare (ignore code reason))
                           (handle-close-connection ws)))
    ;; Send the handshake
    (lambda (responder)
      (declare (ignore responder))
      (websocket-driver:start-connection ws))))

(defvar *chat-handler* (clack:clackup #'chat-server :port 12345))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils https://github.com/rajasegar/cl-warehouse/
(defun get-param (name parsed)
  "Get param values from _parsed"
  (cdr (assoc name parsed :test #'string=)))

(defmacro log-web-error (msg &body body)
  "Broadcast message from log4cl"
  `(let ((str (with-output-to-string
                  (*debug-io*) (log:error ,msg ,@body))))
     (when (not (string-equal "" str))
       (maphash (lambda (con usr)
                  (declare (ignore usr))
                  (websocket-driver:send con str))
                *connections*))))

(defmacro log-web-warn (m &body body)
  "Broadcast message from log4cl"
  `(let ((s (with-output-to-string
                (*debug-io*) (log:warn ,m ,@body))))
     (when (not (string-equal "" s))
       (maphash (lambda (key value)
                  (declare (ignore value))
                  (websocket-driver:send key s))
                *connections*))))

(defmacro log-web-info (m &body body)
  "Broadcast message from log4cl"
  `(let ((s (with-output-to-string
                (*debug-io*) (log:info ,m ,@body))))
     (when (not (string-equal "" s))
       (maphash (lambda (key value)
                  (declare (ignore value))
                  (websocket-driver:send key s))
                *connections*))))

(defmacro log-web-debug (m &body body)
  "Broadcast message from log4cl"
  `(let ((s (with-output-to-string
                (*debug-io*) (log:debug ,m ,@body))))
     (when (not (string-equal "" s))
       (maphash (lambda (key value)
                  (declare (ignore value))
                  (websocket-driver:send key s))
                *connections*))))

(defmacro log-web-trace (m &body body)
  "Broadcast message from log4cl"
  `(let ((s (with-output-to-string
                (*debug-io*) (log:trace ,m ,@body))))
     (when (not (string-equal "" s))
       (maphash (lambda (key value)
                  (declare (ignore value))
                  (websocket-driver:send key s))
                *connections*))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Core logging generator
(defparameter *count* 0)

(defun domore ()
  (log-web-debug "More" *count*)
  (log-web-info "More" *count*)
  (log-web-warn "More" *count*)
  (log-web-error "More" *count*))

(defun dostuff ()
  (log-web-debug "Stuff" *count*)
  (log-web-warn "Stuff" *count*)
  (domore))

(defun runner ()
  ;;(slynk:create-server :port 5555)
  (log-web-info "Here we go!~%")
  (bt:make-thread (lambda ()
                    (do () (nil)
                      (dostuff)
                      (incf *count*)
                      (sleep 5)))
                  :name "Runner thread"))

;; Start loop
(runner)

;; Clean all stuff
;; Caveman uses datafly, that uses log4cl and creates its logger
(setf (log4cl:logger-additivity (log:category '(datafly))) t)
(log4cl:remove-all-appenders (log:category '(datafly)))
;; Reset the *root-logger*
(log4cl:remove-all-appenders log4cl:*root-logger*)
;; :reset removes all loggers that are additive and appenders-void
(log:config :reset)

;; Create an appender to *root-logger* with desired layout
(log4cl:add-appender log4cl:*root-logger*
                     (make-instance
                      'log4cl:console-appender
                      :layout (make-instance
                               'log4cl:pattern-layout
                               :conversion-pattern "<%p> %d %c %m%n")))

;; In general show all logging messages
(log:config :debu9)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Routing rules

;; Need GET method for the starting point ...
(caveman2:defroute ("/" :method :GET) ()
  (render #P"index.html"
          (list :log-config (with-output-to-string (*standard-output*)
                              (log:config)))))

;; ... and POST for forms behavior.
(caveman2:defroute ("/" :method :POST) ()
  (caveman2:redirect "/"))

;; Customize logging
(caveman2:defroute ("/custom-off" :method :POST) ()
  (log:config (log:category 'dostuff) :off)
  (log:config (log:category 'domore) :off)
  (caveman2:redirect "/"))

(caveman2:defroute ("/custom-error" :method :POST) ()
  (log:config (log:category 'dostuff) :error)
  (log:config (log:category 'domore) :error)
  (caveman2:redirect "/"))

(caveman2:defroute ("/custom-warn" :method :POST) ()
  (log:config (log:category 'dostuff) :warn)
  (log:config (log:category 'domore) :warn)
  (caveman2:redirect "/"))

(caveman2:defroute ("/custom-info" :method :POST) ()
  (log:config (log:category 'dostuff) :info)
  (log:config (log:category 'domore) :info)
  (caveman2:redirect "/"))

(caveman2:defroute ("/custom-debug" :method :POST) ()
  (log:config (log:category 'dostuff) :debug)
  (log:config (log:category 'domore) :debug)
  (caveman2:redirect "/"))

(caveman2:defroute ("/custom-trace" :method :POST) ()
  (log:config (log:category 'dostuff) :trace)
  (log:config (log:category 'domore) :trace)
  (caveman2:redirect "/"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Error pages

(defmethod on-exception ((app <web>) (code (eql 404)))
  (declare (ignore app))
  (merge-pathnames #P"_errors/404.html"
                   *template-directory*))
