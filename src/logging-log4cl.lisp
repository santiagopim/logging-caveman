;; ;;; -*- Mode: LISP; indent-tabs-mode nil; Base: 10 -*-
;; ;;; Copyright (C) 2021 Santiago Payà Miralta

;; (ql:quickload '(clack websocket-driver alexandria log4cl))
;; (defpackage "logging-caveman" (:use :cl))
;; (in-package "logging-caveman")

;; (defparameter *count* 0)

;; (defun domore ()
;;   (log:debug "More" *count*)
;;   (log:warn "More" *count*)
;;   (log:info "More" *count*)
;;   (log:error "More" *count*))

;; (defun dostuff ()
;;   ;(format t "Here we are at ~A" *count*)
;;   (log:debug "Stuff" *count*)
;;   (log:warn "Stuff" *count*)
;;   (domore))

;; (defun runner ()
;;            (slynk:create-server :port 5555)
;;            (format t "Here we go!~%")
;;            (bt:make-thread (lambda ()
;;                              (do () (nil)
;;                                (dostuff)
;;                                (incf *count*)
;;                                (sleep 5)))
;;                            :name "TheThread"))

;; ;; Starting status
;; (log:config)

;; ;;(runner) ;;;;;;;;;;;;;;;;;;;;;;;;; Go!

;; ;; Clean all stuff
;; ;; :reset removes all loggers that are additive and appenders-void.
;; (log:config :reset)
;; (log4cl:remove-all-appenders log4cl:*root-logger*)
;; (log:config)

;; (runner)

;; ;; Create an appender to root-logger with desired layout
;; (log4cl:add-appender log4cl:*root-logger*
;;                      (make-instance
;;                       'log4cl:console-appender
;;                       :layout (make-instance
;;                                'log4cl:pattern-layout
;;                                :conversion-pattern "<%p> %d %c %m%n")))

;; (log:config (log:category '(|logging-caveman| dostuff)) :off)
;; (log:config (log:category (list (package-name *package*) 'dostuff)) :off)
;; (log:config (log:category (list (package-name *package*) 'domore)) :off)
;; (log:config (log:category (list (package-name *package*) 'domore)) :error)
;; (log:config (log:category (list (package-name *package*))) :error)

;; (defparameter *l* (with-output-to-string (*debug-io*) (log:error *count*)))
;; (print *l*)

;; ;; Set all again
;; (log:config :reset)
;; (log:config :trace)

;; (defparameter *c* (with-output-to-string (*standard-output*) (log:config)))
;; (print *c*)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (print *package*)
;; (print (package-name *package*))


;; (log:config :sane)
;; (log:config :sane2)

;; ;; With new logger, but does not work without the root-logger
;; (defparameter *lg1* (log4cl:make-logger '(mycategory)))
;; (log:config *lg1* :own :debug)
;; (log4cl:add-appender *lg1* (make-instance
;;                             'log4cl:fixed-stream-appender
;;                             :stream *debug-io*
;;                             :layout (make-instance
;;                                      'log4cl:pattern-layout
;;                                      :conversion-pattern "%d %m%n"))))
;; (setf (log4cl:logger-additivity (log:category '(mycategory))) t)






;; ;; General properties, for ROOT appenders
;; (log:config :off)
;; (log:config :debug)
;; (log:config :warn)
;; (log:config :thread)
;; (log:config :pretty)
;; (log:config :nopretty)
;; (log:config :package)
;; (log:config :nopackage)
;; (log:config :time)
;; (log:config :notime)
;; (log:config :pattern "%p %d %t %c %m%n")
;; (log:config :clear
;;             :sane
;;             :this-console
;;             :pattern "%D{%H:%M:%S} %-5p  <%c{}{}{:downcase}> {%t} %m%n")

;; ;; Delete ROOT appenders
;; (log4cl:remove-all-appenders log4cl:*root-logger*)
;; (log4cl:remove-all-appenders (log:category '(datafly)))
;; (log:config)

;; ;; Recreate ROOT appenders
;; (log:config :sane2)

;; ;; Add and level new loggers by category
;; (log:config (log:category '(cl-user dostuff)) :off)
;; (log:config (log:category '(cl-user domore)) :off)
;; (log:config)
;; (log4cl:logger-children (log:category '(logging-caveman)))

;; ;; Add appender to logger by category
;; (log4cl:add-appender
;;  (log:category '(cl-user domore))
;;  (make-instance 'log4cl:this-console-appender
;;                 :layout (make-instance 'log4cl:simple-layout)))
;; (log:config)

;; ;; Delete ROOT appenders
;; (log4cl:remove-all-appenders log4cl:*root-logger*)
;; (log:config)

;; ;; Modify appender pattern by logger category
;; (log:config (log:category '(cl-user domore)) :pattern "%d %m%n")
;; (log:config)

;; ;; Remove appenders by logger category
;; (log4cl:remove-all-appenders (log:category '(cl-user domore)))
;; (log:config)

;; ;; Write logg *debug-io* output to string
;; (defvar *l* (with-output-to-string (*debug-io*) (log:error 1)))
;; ;;<ERROR> [22:37:24] cl-user () - 1: 1 
;; ;;*L*
;; ;;CL-USER> *l*
;; ;;"<ERROR> [22:37:24] cl-user () - 1: 1 
;; ;;"

;; ;; Some info
;; (print (log4cl:logger-name (log:category '(cl-user dostuff))))
;; (print (log4cl:logger-category (log:category '(cl-user dostuff))))
;; (print (log4cl::logger-state (log:category '(cl-user dostuff))))
;; (print (log4cl-impl:logger-categories (log:category '(cl-user))))
;; (print (log4cl:effective-log-level (log:category '(cl-user))))
;; (print (log4cl:effective-log-level (log:category '(cl-user dostuff))))
;; (print (log4cl:effective-log-level (log:category '(cl-user domore))))

;; ;; Set additivity
;; (setf (log4cl:logger-additivity (log:category '(datafly))) t)
;; (log:config)

;; ;;; Remote control
;; ;;; M-x sly-connect :port 5555
;; ;;; CL-USER> (log:config :this-console)
;; ;;; (log:config :this-console :force-add)


;; ;; From https://groups.google.com/g/comp.lang.lisp/c/lwW-EkB6eA4
;; (time
;;  (with-open-file
;;      (*standard-output* "/dev/null" :direction :output 
;;                                     :if-exists :append) 
;;    (let ((logger (log4cl:make-logger '(mycategory)))) 
;;      (log:config logger :own :debug) 
;;      (log4cl:add-appender 
;;       logger (make-instance
;;               'log4cl:fixed-stream-appender 
;;               :stream *standard-output* 
;;               :layout (make-instance
;;                         'log4cl:pattern-layout 
;;                         :conversion-pattern "%m")))
;;      (log:config)
;;      (dotimes (cnt 10000000) 
;;        (log:debug logger :layout "iter=~d" cnt)) 
;;      (log4cl:remove-all-appenders logger))))


;; ;; With new logger, but does not work without the root-logger
;; (defparameter *lg1* (log4cl:make-logger '(mycategory)))
;; (log:config *lg1* :own :debug)
;; (log4cl:add-appender *lg1* (make-instance
;;                             'log4cl:fixed-stream-appender
;;                             :stream *debug-io*
;;                             :layout (make-instance
;;                                      'log4cl:pattern-layout
;;                                      :conversion-pattern "%d %m%n"))))
;; (setf (log4cl:logger-additivity (log:category '(mycategory))) t)

;; ;; Clean all stuff
;; ;; :reset removes all additive appenders-void loggers
;; (log:config :reset)
;; (log4cl:remove-all-appenders log4cl:*root-logger*)
;; (log:config)

;; (runner)

;; ;; Create an appender to root-logger with desired layout
;; (log4cl:add-appender log4cl:*root-logger*
;;                      (make-instance
;;                       'log4cl:fixed-stream-appender
;;                       :stream *debug-io*
;;                       :layout (make-instance
;;                                'log4cl:pattern-layout
;;                                :conversion-pattern "<%p> %d %c %m%n")))

;; (log:config (log:category '(cl-user logging-caveman.dostuff)) :off)
;; (log:config (log:category '(cl-user logging-caveman.domore)) :off)
;; (log:config (log:category '(logging-caveman dostuff)) :warn)
;; (log:config (log:category '(domore)) :off)

;; (with-output-to-string (*debug-io*) (log:error 1))

;; (log:config :sane2)


;; (log:info *lg1* 'test)
;; (log4cl:remove-all-appenders *lg1*)
;; (log:config :sane)

;; ;;; With log4cl-extras

;; (require :log4cl-extras)

;; ;;; log4cl-extras/config:setup close all loggers:
;; ;; This removes all loggers except ROOT
;; ;;(log:config :reset)
;; ;; This removes appenders from the root logger.
;; ;;(log4cl:remove-all-appenders log4cl:*root-logger*)
;; (log4cl-extras/config:setup '())

;; (log4cl-extras/config:setup
;;  '(:level :debug
;;    :appenders ((this-console
;;                 :filter :warn)
;;                (daily :layout :plain
;;                       :name-format "/home/santiago/log4cl-extras.log"
;;                       :backup-name-format "app-%Y%m%d.log"))))



